Google's Android Kernel Build System
=====================================

Current Device: Husky / Shiba - Pixel 8 Pro / Pixel 8

Current Branch: android-gs-shusky-6.1-android15-qpr2-beta

This is based on google branches and source code.

Everything is currently pulled from google save for a few repositories listed here:

>"android | kernel manifest"
>https://gitlab.com/hdpk/kernel_manifest/-/tree/sh15-6.1

>"anykernel3"
>https://gitlab.com/hdpk/anykernel3/-/tree/sh15-6.1

>"build/kernel"
>https://gitlab.com/hdpk/android_build/-/tree/sh15-6.1

>"aosp | kernel_common"
>https://gitlab.com/hdpk/kernel_common/-/tree/sh15-6.1

>"private/devices/google/common"
>https://gitlab.com/hdpk/devices_google_common/-/tree/sh15-6.1

>"private/devices/google/gs201"
>https://gitlab.com/hdpk/devices_google_gs201/-/tree/sh15-6.1

>"private/devices/google/pantah"
>https://gitlab.com/hdpk/devices_google_pantah/-/tree/sh15-6.1

>"private/devices/google/shusky"
>https://gitlab.com/hdpk/devices_google_shusky/-/tree/sh15-6.1

>"private/devices/google/zuma"
>https://gitlab.com/hdpk/devices_google_zuma/-/tree/sh15-6.1

>"private/google-modules/gpu"
>https://gitlab.com/hdpk/google-modules_gpu/-/tree/sh15-6.1

>"private/google-modules/soc/gs"
>https://gitlab.com/hdpk/google-modules_soc_gs/-/tree/sh15-6.1

>"private/google-modules/wlan/bcm4398"
>https://gitlab.com/hdpk/google-modules_wlan_bcm4398/-/tree/sh15-6.1

>"private/google-modules/wlan/bcm4383"
>https://gitlab.com/hdpk/google-modules_wlan_bcm4383/-/tree/sh15-6.1


Warning: Occasionally things may be changed or force-pushed without notice.

In Order To Build
====================

>mkdir -p p815 && cd p815

>repo init -u https://gitlab.com/hdpk/kernel_manifest -b sh15-6.1

>repo sync -j 8

>time ./build_shusky.sh